<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Products;
use \App\Variations;

class VariationsController extends Controller
{
	public function index($id)
	{
		$variation = Variations::find($id);


    	$product = \DB::table('products')
    		->select('products.*')
    		->join('variations', 'variations.products_id', '=', 'products.id')
    		->where('variations.id', $id)
    		->get()
    		->first();

		return view('variations', compact(['variation', 'product']));
	}
}
