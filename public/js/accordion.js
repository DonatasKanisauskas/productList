$(document).ready(function () {
	$('#categories').on('show.bs.collapse', function () {
		localStorage.setItem('collapseItem', 'true');
	});
	$('#categories').on('hide.bs.collapse', function () {
		localStorage.removeItem('collapseItem');
	});

	var collapseItem = localStorage.getItem('collapseItem');
	if(collapseItem){
		$('#categories').collapse('show');
	}
})