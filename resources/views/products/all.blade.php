@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">
			<div class="card">
				<div class="card-header"><strong>Produktai</strong></div>
				<div class="card-body">

						<div>
							
							<div class="row">

								<div class="col-md-6">
									<a class="btn btn-primary" data-toggle="collapse" href="#categories" role="button" aria-expanded="false" aria-controls="categories">
										Categories
									</a>
								</div>

								<div class="col-md-2 align-self-center">
									Sort:
									<a  href="{{ route('products.filter', ['category' => request('category'), 'sort' => 'desc' ]) }}">▲</a> |
									<a href="{{ route('products.filter', ['category' => request('category'), 'sort' => 'asc' ]) }}">▼</a>
								</div>

								<div class="col-md-4 text-right align-self-center">
									<form action="?search" class="search-container">
										<input type="text" placeholder="Search.." name="search">
										<button type="submit">&#128269</button>
									</form>
								</div>

							</div>

							<form action="{{ route('products.filter') }}" method="get">
								<div class="collapse" id="categories">
									<div class="card card-body">
										<div class="row">

											@foreach( $categories as $category )
												<div class="col-4">
													<div class="form-check">
														<input name="category[]" type="checkbox" onChange="javascript:this.form.submit();" value="{{ $category->id }}"
															@foreach ($checked as $check)
																@if($category->id == $check)
																	checked
																@endif
															@endforeach
														>
														<labeL>{{ $category->name }}</labeL>
													</div>
												</div>
											@endforeach
											<div class="col-4">
												<div class="form-check">
													<a href="/products">Visos</a>
												</div>
											</div>

										</div>
									</div>
								</div>
							</form>

							<hr>

						</div>

					<div class="container">
						<div class="row">
							@foreach( $products as $product )
							<div class="col-4 card-margin">
								<div class="card max-height">
									<div class="card-body text-center">
										<img class="card-img-top" src="{{ $product->img }}" alt="Card image cap">
										<h5 class="card-title">{{ $product->name }}</h5>

										@php
											$variationCount = $variations->where('products_id', $product->products_id)->count()
										@endphp

										@if($variationCount>0)
											<p>variations: {{ $variationCount }}</p>
										@endif

										<a href="/product/{{ $product->products_id }}" class="btn btn-primary">info</a>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>

					<div class="row">
						<div class="col-12 d-flex justify-content-center pt-4">
							{{ $products->links() }}
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
