@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><h3>{{ $product->name }}</h3></div>
                <div class="card-body">

                    <div class="container">
                        <div class="row">
                            <div class="col-4">
                                <img class="card-img-top" src="{{ $product->img }}" alt="Card image cap">
                            </div>
                            <div class="col-4">
                                <h5>kategorijos: </h5>
                                <ul>
                                    @foreach ($categories as $category)
                                        <li>{{ $category->name }}</li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="col-4">
                                @if( count($variations) > 0 )
                                    <h5>variacijos: </h5>
                                    <ul>
                                        @foreach ($variations as $variation)
                                            <li><a href="/variation/{{ $variation->id }}">{{ $variation->name }}</a></li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>

                        </div>
                    </div>

                    <div class="container">
                        <div class="row">

                            <div class="col-12">
                                <h4>Info</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
